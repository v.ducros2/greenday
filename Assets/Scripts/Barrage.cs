﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrage : MonoBehaviourPun
{
    [SerializeField]
    GameObject menuBarrage = null;
    [SerializeField]
    GameObject gameManager = null;
    [SerializeField]
    private int BarrageCost;
    private bool cursorVisibility;

    private void Start()
    {
        menuBarrage.GetComponent<BarrageUI>().gameManager = gameManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.gameObject.GetPhotonView().IsMine)
            {
                Cursor.lockState = CursorLockMode.None;
                cursorVisibility = Cursor.visible;
                Cursor.visible = true;
                menuBarrage.SetActive(true);
                menuBarrage.GetComponent<BarrageUI>().BarrageCost = BarrageCost;
                menuBarrage.GetComponent<BarrageUI>().gameManager = gameManager;
                menuBarrage.GetComponent<BarrageUI>().barrage = this.gameObject;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.gameObject.GetPhotonView().IsMine)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = cursorVisibility;
                menuBarrage.SetActive(false);
            }
        }
    }
}
