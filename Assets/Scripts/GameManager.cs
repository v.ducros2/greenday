﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int money;
    public int barrageBuild;
    public int barrageBuildMax;
    public Vector3 rotationPlanetSpeed = new Vector3(-1.3f,2,1);
    public GameObject planet;

    [SerializeField]
    private GameObject playerUI = null;
    [SerializeField]
    private GameObject endGameUI = null;
    [SerializeField]
    private SpawnDechet spawnDechet = null;
    private int barrageBuildBack;

    private void Awake()
    {
        money = 0;
        barrageBuild = 0;
        barrageBuildBack = barrageBuild;
        barrageBuildMax = 3;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (barrageBuild == barrageBuildMax)
        {
            Debug.Log("beforeCoroutine");
            StartCoroutine(EndGame());
        }
        if(barrageBuild > barrageBuildBack)
        {
            barrageBuildBack = barrageBuild;
            spawnDechet.Clean(barrageBuild, barrageBuildMax);
        }
    }

    private void FixedUpdate()
    {
        if (planet != null)
        {
            planet.transform.Rotate( rotationPlanetSpeed * Time.deltaTime);
        }
    }

    private IEnumerator EndGame()
    {
        Debug.Log("coroutineStart");
        playerUI.SetActive(false);
        endGameUI.SetActive(true);

        

        yield return new WaitForSecondsRealtime(5);
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(sceneName: "Rooms");

    }
}
