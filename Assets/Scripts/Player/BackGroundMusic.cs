﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundMusic : MonoBehaviour
{
    private AudioSource audio;
    private AudioClip[] backGroundSoundArray;
    public bool AutoStart = false;


    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.Play();
        backGroundSoundArray = Resources.LoadAll<AudioClip>("Sounds/Background");
    }

    // Update is called once per frame
    void Update()
    {
        if (!audio.isPlaying && AutoStart)
        {
            audio.clip = backGroundSoundArray[Random.Range(0, backGroundSoundArray.Length)];
            audio.Play();
        }
    }

    public void StopMusic()
    {
        audio.Pause();
    }

    public void ResumMusic()
    {
        audio.Play();
    }

    public void SetVolume(float volume)
    {
        audio.volume = volume;
    }
}
