﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DechetSeparation : MonoBehaviour
{
    public bool CanSeparateDechet = false;
    public bool CanCollectDechet = false;
    public PlayerController playerController;
    public int MaxDechetStackCanBeCollected = 1;


    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "dechet")
        {
            if (CanSeparateDechet)
                other.GetComponent<Class_Dechet>().Unstick();

            if (CanCollectDechet
                && other.GetComponent<Class_Dechet>().GetJointureList().Count <= MaxDechetStackCanBeCollected
                && playerController.stockageMax > playerController.stockageCurrent)
            {
                Destroy(other.gameObject);
                playerController.stockageCurrent++;
                //Update Score
            }
            else
            {
                //damage
            }
        }
    }
}
