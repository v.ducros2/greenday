﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Boat/BoatRole")]
public class PlayerClass : ScriptableObject
{
    public int Life;
    public string BoatRole;
    public int Stockage;
    public int Speed;
    public int LookSensitivity;
}
