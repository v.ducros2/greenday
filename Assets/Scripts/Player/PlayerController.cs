﻿using Photon.Pun;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviourPun
{
    public float speed = 0;
    public float lookSensitivity = 0;
    public int lifeMax = 0;
    public int lifeCurrent = 0;
    public int stockageMax = 0;
    public int stockageCurrent = 0;
    [SerializeField]
    private GameObject gfxBoat = null;


    // affectation du playerClass a l'initialisation du player
    [SerializeField]
    public PlayerClass playerClass = null;

    public GameObject cameraPlaceInit = null;
    private PlayerMotor motor;

    private void Awake()
    {
        if (base.photonView.IsMine)
        {
            motor = GetComponent<PlayerMotor>();

            #region init classe
            speed = playerClass.Speed;
            lookSensitivity = playerClass.LookSensitivity;
            lifeMax = playerClass.Life;
            stockageMax = playerClass.Stockage;
            lifeCurrent = lifeMax;
            Debug.Log(stockageMax);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            #endregion
        }
    }

    void Update()
    {
        if (base.photonView.IsMine)
        {
            #region Deplacement

            //init all data 
            float yRot = 0;
            float xRot = 0;

            float xMov = Input.GetAxisRaw("Horizontal");
            float zMov = Input.GetAxisRaw("Vertical");

            //check if cursor is in game or in menu
            if (Cursor.lockState != CursorLockMode.None)
            {
                //calculate rotation
                yRot = Input.GetAxisRaw("Mouse X");

                //calculate camera rotation
                xRot = Input.GetAxisRaw("Mouse Y");
            }

            //calc vector horirzontal vertical
            Vector3 movHorizontal = transform.right * xMov;
            Vector3 movVertival = transform.forward * zMov;

            //final move vector
            Vector3 velocity = (movHorizontal + movVertival).normalized * speed;

            //apply movement
            motor.Move(velocity);

            // Vecteur pour diriger le personnage
            Vector3 rotation = new Vector3(0f, yRot, 0f) * lookSensitivity;

            //apply rotation
            motor.Rotate(rotation);


            // Vecteur pour diriger le personnage
            Vector3 cameraRotation = new Vector3(xRot, 0f, 0f) * lookSensitivity;

            //apply camera rotation
            motor.RotateCamera(cameraRotation);
            #endregion
        }

    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "islands")
        {
            lifeCurrent--;
        }
    }

    public string TryStockageDechet()
    {
        if (base.photonView.IsMine)
        {
            if (stockageCurrent <= stockageMax)
            {
                return "stockageFull";
            }

            stockageCurrent += 1;
            return "stockageOk";
        }

        return "notMine";
    }
}
