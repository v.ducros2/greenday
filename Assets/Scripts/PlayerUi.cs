﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUi : MonoBehaviour
{

    public GameObject gameManager = null;
    public PlayerController player = null;

    [SerializeField]
    private GameObject lifeUI = null;
    [SerializeField]
    private GameObject stockageUI = null;
    [SerializeField]
    private GameObject objectifUI = null;
    [SerializeField]
    private GameObject moneyUI = null;

    private GameManager game;

    // Start is called before the first frame update
    void Start()
    {
        game = gameManager.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        moneyUI.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = game.money.ToString();
        lifeUI.transform.GetChild(0).GetComponent<Image>().fillAmount = (float)player.lifeCurrent/(float)player.lifeMax;
        stockageUI.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = player.stockageCurrent.ToString()+ " / " + player.stockageMax.ToString();
        objectifUI.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = game.barrageBuild.ToString() + " / " + game.barrageBuildMax.ToString();
    }
}
