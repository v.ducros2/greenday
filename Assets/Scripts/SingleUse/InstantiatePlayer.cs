﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiatePlayer : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> listPrefab = new List<GameObject>();
    [SerializeField]
    private Camera cam;
    [SerializeField]
    private GameObject gameManager;
    [SerializeField]
    private GameObject PlayerUI;

    public void choiceShip(int choice)
    {
        // instanciation du player
        GameObject player = MasterManager.NetworkInstantiate(listPrefab[choice], transform.position, Quaternion.identity);
        // attribution de la cam au player
        player.GetComponent<PlayerMotor>().cam = cam;

        // affiliation de la cam
        cam.transform.parent = player.transform;

        // set position de la cam
        cam.transform.position = player.GetComponent<PlayerController>().cameraPlaceInit.transform.position;

        if (player.gameObject.GetPhotonView().IsMine)
        {
            PlayerUI.GetComponent<PlayerUi>().player = player.GetComponent<PlayerController>();
            PlayerUI.GetComponent<PlayerUi>().gameManager = gameManager;
            PlayerUI.SetActive(true);
            
        }
    }

}
