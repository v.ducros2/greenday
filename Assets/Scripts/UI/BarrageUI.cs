﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BarrageUI : MonoBehaviour
{
    public GameObject gameManager = null;
    public GameObject barrage = null;
    public int BarrageCost;

    [SerializeField]
    private GameObject moneyUI = null;
    [SerializeField]
    private GameObject BarrageCostUI = null;

    private void Update()
    {
        moneyUI.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = gameManager.GetComponent<GameManager>().money.ToString();
        BarrageCostUI.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = BarrageCost.ToString();
    }
    public void InstallBarrage()
    {
        if(gameManager.GetComponent<GameManager>().money >= BarrageCost)
        {
            barrage.transform.GetChild(0).gameObject.SetActive(false);
            barrage.transform.GetChild(1).gameObject.SetActive(true);
            gameManager.GetComponent<GameManager>().barrageBuild += 1;
            gameManager.GetComponent<GameManager>().money -= BarrageCost;
            Destroy(barrage.GetComponent<SphereCollider>());
            this.gameObject.SetActive(false);
        }
    }

}
