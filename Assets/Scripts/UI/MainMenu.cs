﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{

    public GameObject menu;
    public GameObject option;
    public AudioSource audio;
    public UnityEngine.UI.Slider slider;
    private float audiolvl;
    private bool MenuIsOpen = false;
    private CursorLockMode cursorSave;
    private bool cursorVisibility;
    private float timeSpeed;



    //load settings
    private void Start()
    {
        audiolvl = PlayerPrefs.GetFloat("Sound lvl", 1);
        audio.volume = audiolvl;
    }

    void Update()
    {
       if(Input.GetButtonDown("Cancel"))
        {
            if (MenuIsOpen)
            {
                Resume();
                //Time.timeScale = timeSpeed; //only solo
            }
            else
            {
                //timeSpeed = Time.timeScale;
                //Time.timeScale = 0;
                cursorSave = Cursor.lockState;
                cursorVisibility = Cursor.visible;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                Menu();
            }
        }
    }

    public void Resume()
    {
        MenuIsOpen = false;
        menu.SetActive(false);
        option.SetActive(false);
        Cursor.visible = cursorVisibility;
        Cursor.lockState = cursorSave;
    }

    public void OptionMenu()
    {
        if (option != null)
        {
            MenuIsOpen = true;
            menu.SetActive(false);
            option.SetActive(true);
            if (slider != null)
                slider.value = audiolvl;
        }
    }

    public void Menu()
    {
        if (menu != null)
        {
            MenuIsOpen = true;
            option.SetActive(false);
            menu.SetActive(true);
        }
    }

    public void Quit()
    {
        PhotonNetwork.LeaveRoom();
        Application.Quit();
    }

    public void SetAudioLevel(float lvl)
    {
        audio.volume = lvl;
        PlayerPrefs.SetFloat("Sound lvl", lvl);
    }






}
