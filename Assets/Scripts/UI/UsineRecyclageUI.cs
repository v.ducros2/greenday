﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UsineRecyclageUI : MonoBehaviour
{
    public GameObject gameManager = null;
    public PlayerController player = null;

    [SerializeField]
    private GameObject lifeUI = null;
    [SerializeField]
    private GameObject stockageUI = null;
    [SerializeField]
    private GameObject speedUI = null;
    [SerializeField]
    private GameObject moneyUI = null;
    

    private void Update()
    {
        lifeUI.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = player.lifeCurrent.ToString()+ " / " + player.lifeMax.ToString();
        stockageUI.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = player.stockageMax.ToString();
        speedUI.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = player.speed.ToString();
        moneyUI.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = gameManager.GetComponent<GameManager>().money.ToString();
    }

    public void ReparerBoat()
    {
        if (gameManager.GetComponent<GameManager>().money >= 5)
        {
            int lifeMax = player.lifeMax;
            int life = player.lifeCurrent;
            if(life < lifeMax)
            {
                gameManager.GetComponent<GameManager>().money -= 5;
                life += 5;
                if (life > lifeMax)
                {
                    life = lifeMax;
                }

                player.lifeCurrent = life;
            }
        }
    }

    public void UpgradeStockageMaxBoat()
    {
        if (gameManager.GetComponent<GameManager>().money >= 50)
        {
            player.stockageMax += 2;
            gameManager.GetComponent<GameManager>().money -= 50;
        }
    }

    public void UpgradeSpeedMaxBoat()
    {
        if (gameManager.GetComponent<GameManager>().money >= 100)
        {
            player.speed += 1;
            gameManager.GetComponent<GameManager>().money -= 100;
        }
    }

    public void UpgradeLifeMaxBoat()
    {
        if (gameManager.GetComponent<GameManager>().money >= 50)
        {
            player.lifeMax += 5;
            player.lifeCurrent += 5;
            gameManager.GetComponent<GameManager>().money -= 50;
        }
    }
}
