﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UsineRecyclage : MonoBehaviourPun
{
    [SerializeField]
    GameObject menuUsineRecycle = null;
    [SerializeField]
    GameObject gameManager = null;

    private void Start()
    {
        menuUsineRecycle.GetComponent<UsineRecyclageUI>().gameManager = gameManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.gameObject.GetPhotonView().IsMine)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                PlayerController boat = other.gameObject.GetComponent<PlayerController>();

                menuUsineRecycle.GetComponent<UsineRecyclageUI>().player = boat;
                menuUsineRecycle.SetActive(true);

                int stockage = boat.stockageCurrent;
                boat.stockageCurrent = 0;
                gameManager.GetComponent<GameManager>().money += 2 * stockage;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.gameObject.GetPhotonView().IsMine)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                menuUsineRecycle.SetActive(false);
            }
        }
    }

    


}
