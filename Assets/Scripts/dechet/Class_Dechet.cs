﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Class_Dechet : MonoBehaviour
{
    public GameObject dechetMasterObject;
    [SerializeField] private float DispawnTimeDechet = 300;

    [SerializeField] private List<HingeJoint> jointureList;
    [SerializeField] private List<Class_Dechet> class_DechetsList;

    private DechetMaster dechetMaster;
    [SerializeField] private string uuid;
    [SerializeField] private float mass;
    [SerializeField] private float bounciness;
    [SerializeField] private bool freezeRotation;
    [SerializeField] private Vector3 velocity;
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private GameObject masterGameobject { get; set; }
    [SerializeField] private Collider collider;
    [SerializeField] private bool masterbool;
    [SerializeField] private bool UnStickAll = false;

    public bool m_oneTime = true;

    public bool master
    {
        get { return masterbool; }
        set { masterbool = value; }
    }
    [SerializeField]
    private String masterNameString;
    public String masterName
    {
        get { return masterNameString; }
        set { masterNameString = value; }
    }
    [SerializeField]
    private bool slavebool;
    public bool slave
    {
        get { return slavebool; }
        set { slavebool = value; }
    }

    public Class_Dechet()
    {
        uuid = System.Guid.NewGuid().ToString();
    }

    public void Reset_Uuid()
    {
        uuid = System.Guid.NewGuid().ToString();
    }

    public string GetUuid()
    {
        return uuid;
    }

    private Vector3 RandomVector(float min, float max)
    {
        var x = UnityEngine.Random.Range(min, max);
        var y = 0;
        var z = 0;
        return new Vector3(x, y, z);
    }

    //---------------------------------------------------------


    void Reset()
    {
        Reset_Uuid();
        this.gameObject.tag = "dechet";
    }

        public void Start() 
    {
        this.name = "dechet_" + uuid;
        master = false;
        masterName = GetUuid();
        slave = false;
        dechetMasterObject = GameObject.FindGameObjectWithTag("dechet_master");
        masterGameobject = this.gameObject;
        if (dechetMasterObject != null )
        { 
            dechetMaster = dechetMasterObject.GetComponent<DechetMaster>();
        }
        else
        {
            Debug.Log("Erreur: Dechet_Master is null");
        }
        rigidbody = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();

        dechetMaster.UpdateNbrDechet(1);


        if(DispawnTimeDechet > 0)
        {
            StartCoroutine(DispawnDelay(DispawnTimeDechet));
        }
    }

    public void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "dechet" && (other.gameObject.GetComponent<Class_Dechet>().masterName != masterName))
        {
            Debug.Log("collider OK:" + other.gameObject.name.ToString());

            if (!other.gameObject.GetComponent<Class_Dechet>().slave && !slave)
                while ((other.gameObject.GetComponent<Class_Dechet>().master && master || (!other.gameObject.GetComponent<Class_Dechet>().master && !master)))
                {
                    if (jointureList.Count == 0)
                    {
                        UpdateMasterRole();
                        other.gameObject.GetComponent<Class_Dechet>().UpdateMasterRole();
                    }
                    else
                        master = false;

                }


            if (slave || (!other.gameObject.GetComponent<Class_Dechet>().master && master))
            {
                //if (!slave)
                //{
                Debug.Log("slave: Direct Link master ");
                other.gameObject.GetComponent<Class_Dechet>().masterName = GetUuid();
                other.gameObject.GetComponent<Class_Dechet>().masterGameobject = this.gameObject;
                HingeJoint jointure = gameObject.AddComponent(typeof(HingeJoint)) as HingeJoint;
                jointure.connectedBody = other.rigidbody;
                other.gameObject.GetComponent<Class_Dechet>().jointureList.Add(jointure);
                jointureList.Add(jointure);
                class_DechetsList.Add(other.gameObject.GetComponent<Class_Dechet>());
                //}
                // else
                //{
                //    Debug.Log("slave: Direct link to another slave " + other.gameObject.name);
                //    other.gameObject.GetComponent<Class_Dechet>().masterName = GetUuid();
                //    HingeJoint jointure = other.gameObject.AddComponent(typeof(HingeJoint)) as HingeJoint;
                //    jointure.connectedBody = other.rigidbody;
                //    AddJointureList(jointure);
                //    other.gameObject.GetComponent<Class_Dechet>().masterGameobject = masterGameobject;
                //collider.isTrigger = true;

                //}
                other.gameObject.GetComponent<Class_Dechet>().master = false;
                other.gameObject.GetComponent<Class_Dechet>().slave = true;
                SaveStick();
                other.gameObject.GetComponent<Class_Dechet>().SaveStick();

            }

            //mass = rigidbody.mass;
            //rigidbody.mass = 0;


        }
        /*else
            if (other.gameObject.GetComponent<Class_Dechet>().masterName != masterName)
        {
            List<HingeJoint> jointure = masterGameobject.GetComponent<Class_Dechet>().GetJointureList();
            for (int i = 0; i < jointure.Count; i++)
            {
                jointure[i].gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                jointure[i].gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                jointure[i].gameObject.GetComponent<Rigidbody>().Sleep();
            }
        }*/

    }

    public void AddJointureList(HingeJoint joint)
    {
        jointureList.Add(joint);
    }

    public List<HingeJoint> GetJointureList()
    {
        return  jointureList;
    }

    public void UpdateMasterRole()
    {
        if (UnityEngine.Random.Range(0, 2) == 1)
            master = true;
        else
            master = false;
    }

    public void SaveStick()
    {
        Debug.Log("Freeze");
        bounciness = collider.material.bounciness;
        collider.material.bounciness = 0;
        //freezeRotation = rigidbody.freezeRotation;
        //rigidbody.freezeRotation = true;
        velocity = rigidbody.velocity;
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
        rigidbody.Sleep();
    }

    public void RestorStickSave()
    {
        masterName = GetUuid();
        master = false;
        slave = false;
        rigidbody.mass = mass;
        collider.material.bounciness = bounciness;
        //rigidbody.freezeRotation = freezeRotation;
        rigidbody.velocity = velocity;
    }

    public void Unstick()
    {

        for (int i = 0; i < jointureList.Count; i++)
        {
            jointureList.RemoveAt(i);
            Destroy(jointureList[i]);
            class_DechetsList[i].jointureList.Remove(jointureList[i]);
            class_DechetsList[i].RestorStickSave();
            class_DechetsList.RemoveAt(i);
        }
        RestorStickSave();
    }


    public void jointureUpdate()
    {
        for (int i = 0; i < jointureList.Count; i++)
            if (jointureList[i] == null)
                jointureList.RemoveAt(i);

        for (int i = 0; i < class_DechetsList.Count; i++)
            if (class_DechetsList[i] == null)
                class_DechetsList.RemoveAt(i);
    }

    public void Update()
    {
        if(UnStickAll)
        {
            Unstick();
            UnStickAll = false;
        }

        if (m_oneTime)
        {
            GetComponent<Rigidbody>().AddForce(new Vector3(5.0f, 0.0f, 0.0f), ForceMode.Impulse);
            m_oneTime = false;
        }

        jointureUpdate();
    }

    public void Dispawn()
    {
        Unstick();
        Destroy(this.gameObject);
        Debug.Log("Dispawn: " + "dechet_" + uuid);
    }

    IEnumerator DispawnDelay(float time)
    {
        yield return new WaitForSeconds(time);
        Unstick();
        Destroy(this.gameObject);
        Debug.Log("Dispawn: " + "dechet_" + uuid);
    }



}
