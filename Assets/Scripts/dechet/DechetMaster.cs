﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DechetMaster : MonoBehaviour
{
    [SerializeField] private int nbrDechet = 0;

    public void UpdateNbrDechet(int addNbrDechet)
    // Add or remove nbr dechet from register
    {
        nbrDechet = GameObject.FindGameObjectsWithTag("dechet").Length;
    }

    public int GetNbrDechet()
    // get nbr dechet
    { return nbrDechet; }


}
