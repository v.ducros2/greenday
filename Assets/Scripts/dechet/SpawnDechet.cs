﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDechet : MonoBehaviour
{

    public int numObjects = 10;
    public float raduisSphere = 10f;
    public int nbrMaxDechet = 300;
    public bool increasseInTime = true;
    public int increasseInTimeSecond = 1;
    public int increasseInTimeNbr = 1;

    [SerializeField]
    private GameObject[] dechetList;

    [SerializeField]
    private bool DrawZoneSpawn = false;

    // Start is called before the first frame update
    void Start()
    {
        dechetList = Resources.LoadAll<GameObject>("Dechets");
        StartCoroutine(IncreaseInTime());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameObject.FindGameObjectsWithTag("dechet").Length < numObjects)
        {
            spawn();
        }

        if (GameObject.FindGameObjectsWithTag("dechet").Length > numObjects)
        {
            GameObject.FindGameObjectsWithTag("dechet")[0].GetComponent<Class_Dechet>().Dispawn();
        }
    }

    void spawn()
    {
        Vector3 pos = RandomCircle(transform.position, raduisSphere);
        Quaternion rot = Quaternion.FromToRotation(Vector3.forward, transform.position - pos);
        GameObject dechet = Instantiate(dechetList[Random.Range(0, dechetList.Length)], pos, rot);
        dechet.GetComponent<Class_Dechet>().Reset_Uuid();
    }

    public void Clean(int nbrBuild, int nbrBuildMax)
    {

    }


    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        if (DrawZoneSpawn)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.position, raduisSphere);
        }
    }


    Vector3 RandomCircle(Vector3 center, float radius)
    {
        return new Vector3(Random.insideUnitSphere.x * radius + center.x,
                 Random.insideUnitSphere.y * radius + center.y,
                 Random.insideUnitSphere.z * radius + center.z);
    }

    private IEnumerator IncreaseInTime()
    {
        while(increasseInTime)
        {
            if(nbrMaxDechet >= numObjects)
            numObjects += increasseInTimeNbr;
            yield return new WaitForSeconds(increasseInTimeSecond);
        }
    }


}