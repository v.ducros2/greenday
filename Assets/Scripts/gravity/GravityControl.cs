﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityControl : MonoBehaviour
{

    public GravityOrbit Gravity;
    private Rigidbody Rb;

    public float RoationSpeed = 20;

    void Start()
    {
        Rb = this.GetComponent<Rigidbody>();
        //Rb.constraints = RigidbodyConstraints.FreezeRotation;
        Rb.useGravity = false;
        Rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {

        if (Gravity)
        {
            Vector3 gravityUp = Vector3.zero;

            if (Gravity.FixedDirection)
            {
                gravityUp = Gravity.transform.up;
            }
            else
            {
                gravityUp = (transform.position - Gravity.transform.position).normalized;
            }
            Quaternion targetRotation = Quaternion.FromToRotation(Rb.transform.up, gravityUp) * Rb.rotation;
            Rb.MoveRotation(targetRotation);

            Rb.AddForce((-gravityUp * Gravity.Gravity) * Rb.mass);


        }
    }
}
