﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityOrbit : MonoBehaviour
{
    // Start is called before the first frame update
    public float Gravity;
    public bool FixedDirection;

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<GravityControl>())
        {
            other.GetComponent<GravityControl>().Gravity = this.GetComponent<GravityOrbit>();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "dechet")
            other.gameObject.GetComponent<Class_Dechet>().Dispawn();
    }

}
